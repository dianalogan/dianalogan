# Diana's README

Hi, I'm Diana. I'm a Senior Technical Writing Manager at GitLab. I use she/her pronouns.

# Who I am

- I was born in the UK in the beautiful county of [Somerset](https://en.wikipedia.org/wiki/Somerset).
- I now live north of London, in rural [Bedfordshire](https://en.wikipedia.org/wiki/Bedfordshire) with my husband and our two teenagers.
- I received a degree in German and History from the University of London, Queen Mary College.
- I started my career in Munich, Germany at a CRM (Customer Relationship Management) software startup.
- My first role was in implementation consulting and training. I spent most of my time onsite at enterprise customers in Europe, North America, and Asia.
- When I wasn't travelling, I was writing software documentation, training materials, and implementation guides.
- I saw the transformative power of good documentation and moved into Technical Writing full time.
- I've been leading remote and hybrid technical writing teams in a Git and docs as code environment since 2016.
- I joined GitLab in April 2021.  

# Working with me

- I am a morning person, and like to use this time for focused work.
- I appreciate context. I want to know why the work is important, and how it connects to the bigger picture of what we are trying to achieve.
- [Big, hairy, ambitious goals](https://www.jimcollins.com/concepts/bhag.html) motivate me, and keep me engaged. 
- I'm an optmistic pragmatist, and look for small ways to move forward. This trait helps me deal with ambiguity and complexity.
- I'm a manager who has grown to love being a manager. I am energized by the team members that I support, and I love seeing them grow and develop their strengths.
- If I'm quiet, it's because I'm thinking before I speak. I appreciate async work practices that help me articulate my thoughts in writing first.
- I'm highly analytical, and enjoy finding broader themes and patterns in the world around me. I can also overthink things. If you see this happening, feel free to nudge me to action.
- I am skilled at leading difficult conversations. Managers and mentors past and present have invested in me, and continue to be intrinsic to the growth of this skill.

<p>
<details>
<summary>Click for more info!</summary>

## Personality test insights and leadership style
- 16 Personalities: [ISFJ-A Defender-Assertive](https://www.16personalities.com/isfj-personality).
- Leadership style: [situational](https://situational.com/blog/the-four-leadership-styles-of-situational-leadership/). See also Sid's [blog](https://about.gitlab.com/blog/2021/11/19/situational-leadership-strategy/).
- [Crucial Conversations](https://about.gitlab.com/handbook/leadership/crucial-conversations/) made a lasting impact on me as a leader. 

## My passions
- Triathlon. I'm an amateur triathlete, learning all the things swim/bike/run.
- Being outdoors in nature. I love mountains and beaches, but just sitting in the garden is also great.
- Family time. Movie night, vacations, or a shared meal.

</details>
</p>
